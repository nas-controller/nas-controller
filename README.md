# NASController
Welcome to this README file.

## Description
NAS Controller allows you to turn on and shutdown, or reboot, your NAS machine from your smartphone.

## Preparation
NAS Controller will only work if you are in the same local network as your TrueNAS Scale machine. For the app to work you also have to activate Wake On Lan on your NAS server and enable SSH on your NAS machine with at least one user that can issue Shutdown and Reboot commands without having to type their password again.
The first time you use the app you need to go to settings and fill in the following information:

- MAC Address of your NAS Machine;
- SSH Username;
- SSH Password;
- IP Address of your NAS Machine;
- Broadcast Address of your network;
- SSH Port;

This data is encrypted with AES256 key in Shared Preferences.

#Libraries
This project uses the JSCH library for SSH over Java. 
Wake On Lan code was also inspired on the Simple Java Implementation of Wake-on-LAN from Jibble.org.

## Contributing
Open to contributions, especially if it's to add new features or UI improvements.

##Improvements needed

- [ ] Have the app automatically detect broadcast IP without the user having to configure it manually;
- [ ] UI improvements;
- [ ] Use fragments instead of two separate activities for the Main Activity and Settings Activity;
- [ ] Find an alternative to root over SSH for the Shutdown and Reboot features;

## Authors and acknowledgment
Developed by LN.
