package gentsoft.be.nascontroller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class MainActivity extends AppCompatActivity {

    FloatingActionButton btn_settings;
    SharedPreferences sharedPreferences;
    Button btnStart, btnStop, btnReboot;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        btn_settings = findViewById(R.id.btn_settings);
        btnStart = findViewById(R.id.btnStart);
        btnReboot = findViewById(R.id.btnReboot);
        btnStop = findViewById(R.id.btnStop);

        //Start
        btnStart.setOnClickListener(view -> {
            sharedPreferences = getSharedPreferences("settings_NAS", Context.MODE_PRIVATE);
            makeSharedEncrypt();
            final String broadcastip = sharedPreferences.getString(getString(R.string.broadcast_ip), "");
            final String mac = sharedPreferences.getString(getString(R.string.mac_address), "");
            Thread thread = new Thread(() -> {
                try {
                    WakeOnLan.main(new String[]{broadcastip, mac});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        });

       //Shutdown
        btnStop.setOnClickListener(view -> {
            sharedPreferences = getSharedPreferences("settings_NAS", Context.MODE_PRIVATE);
            Thread thread2 = new Thread(() -> {
                        try {
                            makeSharedEncrypt();
                            JSch jsch = new JSch();
                            Session session = jsch.getSession(sharedPreferences.getString(getString(R.string.ssh_username), " "), sharedPreferences.getString(getString(R.string.ip_address), ""), Integer.parseInt(sharedPreferences.getString(getString(R.string.sshPort), "")));
                            session.setPassword(sharedPreferences.getString(getString(R.string.ssh_password), ""));
                            session.setConfig("StrictHostKeyChecking", "no");
                            session.setTimeout(100000);
                            session.connect();
                            ChannelExec channel = (ChannelExec) session.openChannel("exec");
                            channel.setCommand("sudo shutdown now");
                            channel.connect();
                            channel.disconnect();
                        } catch (JSchException e) {
                            e.printStackTrace();
                        }
                    });
                thread2.start();
            });

        //Reboot
        btnReboot.setOnClickListener(view -> {
            sharedPreferences = getSharedPreferences("settings_NAS", Context.MODE_PRIVATE);
            Thread thread3 = new Thread(() -> {
                try {
                    makeSharedEncrypt();
                    JSch jsch = new JSch();
                    Session session = jsch.getSession(sharedPreferences.getString(getString(R.string.ssh_username), " "), sharedPreferences.getString(getString(R.string.ip_address), ""), Integer.parseInt(sharedPreferences.getString(getString(R.string.sshPort), "")));
                    session.setPassword(sharedPreferences.getString(getString(R.string.ssh_password), ""));
                    session.setConfig("StrictHostKeyChecking", "no");
                    session.setTimeout(100000);
                    session.connect();
                    ChannelExec channel = (ChannelExec) session.openChannel("exec");
                    channel.setCommand("sudo reboot now");
                    channel.connect();
                    channel.disconnect();
                } catch (JSchException e) {
                    e.printStackTrace();
                }
            });
            thread3.start();
        });


        //Go to settings to save data
        btn_settings.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, Settings.class);
            startActivity(intent);
        });

    }
    public void makeSharedEncrypt() {
        String masterKeyAlias = null;
        try {
            masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }

        try {
            sharedPreferences = EncryptedSharedPreferences.create(
                    "settings_NAS",
                    masterKeyAlias,
                    this,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            );
        } catch (GeneralSecurityException | IOException e){
            e.printStackTrace();
        }
    }

}