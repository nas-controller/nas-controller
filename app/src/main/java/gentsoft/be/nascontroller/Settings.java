package gentsoft.be.nascontroller;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class Settings extends AppCompatActivity {

    FloatingActionButton btnHome, btn_Save, btn_Load;
    EditText macAddress, sshUsername, txtIpAddress, sshPassword, sshPort, broadcastIp;
    ImageButton btnShowPwd;

    SharedPreferences sharedPreferences;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);


        btnHome = (FloatingActionButton)findViewById(R.id.btnHome);
        btn_Save = (FloatingActionButton)findViewById(R.id.btn_save);
        btn_Load = (FloatingActionButton)findViewById(R.id.btnLoad);
        macAddress = (EditText)findViewById(R.id.macAddress);
        sshUsername = (EditText)findViewById(R.id.sshUsername);
        txtIpAddress = (EditText)findViewById(R.id.txtIpAddress);
        sshPassword = (EditText)findViewById(R.id.sshPassword);
        sshPort = (EditText)findViewById(R.id.sshPort);
        broadcastIp = (EditText)findViewById(R.id.broadcastIp);
        btnShowPwd = (ImageButton)findViewById(R.id.btnShowPwd);


        sharedPreferences = getSharedPreferences("settings_NAS", MODE_PRIVATE);

        //Show password
        btnShowPwd.setOnTouchListener((v, event) -> {
            switch ( event.getAction() ) {
                case MotionEvent.ACTION_UP:
                    sshPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
                case MotionEvent.ACTION_DOWN:
                    sshPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                    break;
            }
            return true;
        });


        //Save settings to file
        btn_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeSharedEncrypt();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(getString(R.string.mac_address), macAddress.getText().toString());
                editor.putString(getString(R.string.ssh_username), sshUsername.getText().toString());
                editor.putString(getString(R.string.ssh_password), sshPassword.getText().toString());
                editor.putString(getString(R.string.ip_address), txtIpAddress.getText().toString());
                editor.putString(getString(R.string.broadcast_ip), broadcastIp.getText().toString());
                editor.putString(getString(R.string.sshPort), sshPort.getText().toString());
                editor.apply();

            }
        });

        //Load settings from file
        btn_Load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeSharedEncrypt();
                String storedMacAddress = sharedPreferences.getString(getString(R.string.mac_address), "");
                String storedSshUserName = sharedPreferences.getString(getString(R.string.ssh_username), "");
                String storedSshPassword = sharedPreferences.getString(getString(R.string.ssh_password), "");
                String storedIpAddress = sharedPreferences.getString(getString(R.string.ip_address), "");
                String storedBroadcastIp = sharedPreferences.getString(getString(R.string.broadcast_ip), "");
                String storedSSHPort = sharedPreferences.getString(getString(R.string.sshPort), "");
                macAddress.setText(storedMacAddress);
                sshUsername.setText(storedSshUserName);
                sshPassword.setText(storedSshPassword);
                txtIpAddress.setText(storedIpAddress);
                broadcastIp.setText(storedBroadcastIp);
                sshPort.setText(storedSSHPort);

            }
        });

        //Return to control screen
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void makeSharedEncrypt() {
        String masterKeyAlias = null;
        try {
            masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }

        try {
            sharedPreferences = EncryptedSharedPreferences.create(
                    "settings_NAS",
                    masterKeyAlias,
                    this,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            );
        } catch (GeneralSecurityException | IOException e){
            e.printStackTrace();
        }
    }

}